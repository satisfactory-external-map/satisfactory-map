using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace SatisfactoryMap.Tests
{
    [TestClass]
    public class TranslationTests
    {
        [TestMethod]
        public void TranslateWorldCoordinateToCanvas()
        {
            Position worldPosition = new Position
            {
                x = -7465.73f,
                y = 244416.34f
            };

            Position expectedCanvasPosition = new Position
            {
                x = 2140f,
                y = 4120
            };

            Position translatedPosition = MainWindow.WorldPositionToCanvasPosition(worldPosition, 5000, 5000);
            double xDelta = Math.Abs(translatedPosition.x - expectedCanvasPosition.x);
            double yDelta = Math.Abs(translatedPosition.y - expectedCanvasPosition.y);
            Assert.IsTrue(xDelta < 50f);
            Assert.IsTrue(yDelta < 50f);
        }
    }
}
