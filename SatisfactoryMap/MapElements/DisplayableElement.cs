﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SatisfactoryMap.MapElements
{
    internal class DisplayableElement
    {
        public string Class { get; set; } = "Miscelaneous";
        public string Tooltip { get; set; } = "";
        public Uri IconUri { get; set; }
        public Position WorldPosition { get; set; }
    }
}
