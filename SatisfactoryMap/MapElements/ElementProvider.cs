﻿using SatisfactorySaveParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SatisfactoryMap.MapElements
{
    class ElementProvider
    {
        private SatisfactorySave _save;
        private IEnumerable<SaveEntity> _saveEntities;

        private IEnumerable<DisplayableElement>  SlugsPurple => _saveEntities
            .FilterByType("/Game/FactoryGame/Resource/Environment/Crystal/BP_Crystal_mk3.BP_Crystal_mk3_C")
            .IntoDisplayable("Slug (Purple)", "resources/static/slug_purple_active.png");
        private IEnumerable<DisplayableElement> SlugsYellow => _saveEntities
            .FilterByType("/Game/FactoryGame/Resource/Environment/Crystal/BP_Crystal_mk2.BP_Crystal_mk2_C")
            .IntoDisplayable("Slug (Yellow)", "resources/static/slug_yellow_active.png");
        private IEnumerable<DisplayableElement> SlugsGreen => _saveEntities
            .FilterByType("/Game/FactoryGame/Resource/Environment/Crystal/BP_Crystal.BP_Crystal_C")
            .IntoDisplayable("Slug (Green)", "resources/static/slug_green_active.png");

        public ElementProvider(byte[] savefile)
        {
            using (MemoryStream ms = new MemoryStream(savefile))
            using (StreamReader sr = new StreamReader(ms))
                _save = new SatisfactorySave(sr);
            _saveEntities = _save.Entries.Where(entry => entry is SaveEntity).Select(entry => entry as SaveEntity);
        }

        public IEnumerable<DisplayableElement> GetDisplayableElements()
        {
            List<DisplayableElement> elements = new List<DisplayableElement>();

            elements.AddRange(SlugsPurple);
            elements.AddRange(SlugsYellow);
            elements.AddRange(SlugsGreen);
            
            return elements;
        }


    }

    internal static class EntityExtensions
    {
        internal static IEnumerable<SaveEntity> FilterByType(this IEnumerable<SaveEntity> inp, string type) => inp.Where(entry => entry.TypePath == type);
        internal static IEnumerable<DisplayableElement> IntoDisplayable(this IEnumerable<SaveEntity> inp, string className, string iconPath)
        {
            Uri uri = new Uri(iconPath, UriKind.Relative);
            return inp.Select(entity =>
            {
                return new DisplayableElement
                {
                    Class = className,
                    Tooltip = className,
                    IconUri = uri,
                    WorldPosition = new Position
                    {
                        x = entity.Position.X,
                        y = entity.Position.Y
                    }
                };
            });
        }
    }
}
