﻿using Newtonsoft.Json;
using SatisfactoryMap.MapElements;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfAnimatedGif;

namespace SatisfactoryMap
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BitmapImage MapImage { get; } = new BitmapImage(new Uri("resources/map_layers/base.png", UriKind.Relative));
        private string[] _playerBlipFilenames = (new DirectoryInfo("resources/player_blips/")).EnumerateFiles().Where(el => el.Extension == ".gif").Select(el => $"resources/player_blips/{el.Name}").ToArray();
        private List<FrameworkElement> BlipElements { get; } = new List<FrameworkElement>();
        private List<FrameworkElement> NodeElements { get; } = new List<FrameworkElement>();
        private List<PresentPlayerBlip> PresentPlayerBlips { get; } = new List<PresentPlayerBlip>();

        public MainWindow()
        {
            InitializeComponent();
            DrawMapImage(mapCanvas);

            new Thread(() => BlipRetrievalLoop()).Start();
            new Thread(() => SavefileRetrievalLoop()).Start();
        }

        private void DrawMapImage(Canvas canvas)
        {
            BitmapImage image = MapImage;
            ImageBrush brush = new ImageBrush(image);
            canvas.Width = image.Width;
            canvas.Height = image.Height;
            canvas.Background = brush;
        }

        private void DrawBlips(Canvas canvas, Blip[] blips)
        {
            foreach(Blip blip in blips)
            {
                Position projectedPosition = WorldPositionToCanvasPosition(blip.position, MapImage.Width, MapImage.Height);
                if (PresentPlayerBlips.Any(el => el.Username == blip.name))
                {
                    // blip already present
                    PresentPlayerBlip ppb = PresentPlayerBlips.First(el => el.Username == blip.name);
                    Canvas.SetLeft(ppb.Control, projectedPosition.x - ppb.Width / 2);
                    Canvas.SetTop(ppb.Control, projectedPosition.y - ppb.Height / 2);
                } else
                {
                    // new blip
                    int playerIconIndex = GetNumberFromString(blip.name, _playerBlipFilenames.Length);
                    string playerIconFilename = _playerBlipFilenames[playerIconIndex];
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.UriSource = new Uri(playerIconFilename, UriKind.Relative);
                    image.EndInit();
                    double targetWidth = image.Width / 8;
                    double targetHeight = image.Height / 8;
                    Image imageControl = new Image
                    {
                        Width = targetWidth,
                        Height = targetHeight
                    };
                    PresentPlayerBlip ppb = new PresentPlayerBlip
                    {
                        Username = blip.name,
                        Control = imageControl,
                        Width = targetWidth,
                        Height = targetHeight
                    };
                    PresentPlayerBlips.Add(ppb);
                    ImageBehavior.SetAnimatedSource(imageControl, image);
                    ImageBehavior.SetRepeatBehavior(imageControl, RepeatBehavior.Forever);
                    ImageBehavior.GetAnimationController(imageControl).Play();
                    Canvas.SetLeft(imageControl, projectedPosition.x - targetWidth / 2);
                    Canvas.SetTop(imageControl, projectedPosition.y - targetHeight / 2);
                    canvas.Children.Add(imageControl);
                    BlipElements.Add(imageControl);
                }
            }
            List<PresentPlayerBlip> toRemove = new List<PresentPlayerBlip>();
            foreach(PresentPlayerBlip blip in PresentPlayerBlips)
            {
                if(!blips.Any(el => el.name == blip.Username))
                {
                    // non-existent blip
                    toRemove.Add(blip);
                    canvas.Children.Remove(blip.Control);
                    BlipElements.Remove(blip.Control);
                }
            }
            PresentPlayerBlips.RemoveAll(el => toRemove.Contains(el));

            canvas.Dispatcher.Invoke(delegate { }, DispatcherPriority.Render);
        }

        public static Position WorldPositionToCanvasPosition(Position worldPosition, double canvasWidth, double canvasHeight)
        {
            // get two known positions
            Position knownPositionOneIngame = new Position
            {
                x = -48431.8f,
                y = 128762.4f
            };
            Position knownPositionOneCanvas = new Position
            {
                x = 1841f,
                y = 3359f
            };

            Position knownPositionTwoIngame = new Position
            {
                x = -174139.9f,
                y = 180833.6f
            };
            Position knownPositionTwoCanvas = new Position
            {
                x = 1002f,
                y = 3705f
            };


            double xDeltaIngame = knownPositionTwoIngame.x - knownPositionOneIngame.x;
            double xDeltaCanvas = knownPositionTwoCanvas.x - knownPositionOneCanvas.x;
            double xCoefficient = xDeltaCanvas / xDeltaIngame;
            double xDeltaIngameGiven = worldPosition.x - knownPositionOneIngame.x;
            double xDeltaCanvasGiven = xDeltaIngameGiven * xCoefficient;
            double xCanvasGiven = xDeltaCanvasGiven + knownPositionOneCanvas.x;

            double yDeltaIngame = knownPositionTwoIngame.y - knownPositionOneIngame.y;
            double yDeltaCanvas = knownPositionTwoCanvas.y - knownPositionOneCanvas.y;
            double yCoefficient = yDeltaCanvas / yDeltaIngame;
            double yDeltaIngameGiven = worldPosition.y - knownPositionOneIngame.y;
            double yDeltaCanvasGiven = yDeltaIngameGiven * yCoefficient;
            double yCanvasGiven = yDeltaCanvasGiven + knownPositionOneCanvas.y;

            double calibratedCanvasWidth = 5000;
            double calibratedCanvasHeight = 5000;

            return new Position
            {
                x = xCanvasGiven / calibratedCanvasWidth * canvasWidth,
                y = yCanvasGiven / calibratedCanvasHeight * canvasHeight
            };
        }

        private static int GetNumberFromString(string inp, int ceil)
        {
            int char_sum = inp.ToCharArray().Select(el => (int)el).Sum();
            Random rand = new Random(char_sum);
            return rand.Next(0, ceil);
        }

        private void BlipRetrievalLoop()
        {
            HttpClient client = new HttpClient();
            while(true)
            {
                Task<string> t = client.GetStringAsync("http://torsten.cz:29007/retrieve/427");
                t.Wait();
                string responseStr = t.Result;
                Response response = JsonConvert.DeserializeObject<Response>(responseStr);

                try
                {
                    Dispatcher.Invoke(() =>
                    {
                        DrawBlips(mapCanvas, response.blips);
                        mapCanvas.InvalidateVisual();
                    });
                }
                catch
                {
                    break;
                }
            }
        }

        private void SavefileRetrievalLoop()
        {
            HttpClient client = new HttpClient();
            while(true)
            {
                Task<byte[]> t = client.GetByteArrayAsync("http://torsten.cz:29007/retrieve/427/savefile");
                t.Wait();
                byte[] savebytes = t.Result;

                ElementProvider provider = new ElementProvider(savebytes);
                IEnumerable<DisplayableElement> elements = provider.GetDisplayableElements();

                try
                {
                    Dispatcher.Invoke(() =>
                    {
                        DrawDisplayableElements(mapCanvas, elements);
                        mapCanvas.InvalidateVisual();
                    });
                }
                catch
                {
                    break;
                }

                Thread.Sleep(60 * 1000);
            }
        }

        private void DrawDisplayableElements(Canvas canvas, IEnumerable<DisplayableElement> elements)
        {
            foreach (FrameworkElement element in NodeElements)
                canvas.Children.Remove(element);
            NodeElements.Clear();

            foreach (DisplayableElement element in elements)
            {
                BitmapImage image = new BitmapImage(element.IconUri);
                Image imageControl = new Image
                {
                    Source = image,
                    Width = image.Width,
                    Height = image.Height
                };
                Position projectedPosition = WorldPositionToCanvasPosition(element.WorldPosition, MapImage.Width, MapImage.Height);
                Canvas.SetLeft(imageControl, projectedPosition.x - image.Width / 2);
                Canvas.SetTop(imageControl, projectedPosition.y - image.Height / 2);
                canvas.Children.Add(imageControl);
                NodeElements.Add(imageControl);
            }

            canvas.Dispatcher.Invoke(delegate { }, DispatcherPriority.Render);
        }
    }

    public class Response
    {
        public uint id;
        public Blip[] blips;
    }

    public class Blip
    {
        public Position position;
        public float heading;
        public string name;
    }

    public class Position
    {
        public double x;
        public double y;
    }

    public struct PresentPlayerBlip
    {
        public string Username;
        public Image Control;
        public double Width;
        public double Height;
    }
}
